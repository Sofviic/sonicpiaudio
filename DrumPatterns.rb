use_debug false
t = 0
live_loop :tick do
  sleep 0.1
  t=t+1
  puts "t = ", t
  puts "t%32 = ", t % 32
end
live_loop :ambiance do
  sync :tick
end
live_loop :mainline do
  sync :tick
end

tomtom = (ring 0)
rimrim = (ring 0)
hihatd = (ring 0)
hihatc = (ring 0)
clapss = (ring 0)
snares = (ring 0)
kickss = (ring 0)

live_loop :drums do
  sync :tick
  pitch_highness = 1
  
  reset_pattern
  footwork_pattern
  
  puts kickss
  
  sample :drum_tom_mid_soft, rate: pitch_highness if tomtom[t] >= 1
  sample :drum_snare_hard, amp: 0.1, rate: pitch_highness if rimrim[t] >= 1
  sample :drum_tom_hi_soft, pitch: :d, rate: pitch_highness if hihatd[t] >= 1
  sample :drum_tom_hi_soft, pitch: :c, rate: pitch_highness if hihatc[t] >= 1
  sample :drum_cymbal_closed, rate: pitch_highness if clapss[t] >= 1
  sample :drum_snare_soft, rate: pitch_highness if snares[t] >= 1
  sample :drum_heavy_kick, rate: pitch_highness if kickss[t] >= 1
end
source = "https://www.pinterest.com/pin/377176537526919823/"
sourceimg = "https://i.pinimg.com/originals/eb/98/a4/eb98a4e0047e4894be1b5045a2d18f90.png"
define :reset_pattern do
  tomtom = (ring 0)
  rimrim = (ring 0)
  hihatd = (ring 0)
  hihatc = (ring 0)
  clapss = (ring 0)
  snares = (ring 0)
  kickss = (ring 0)
end
define :zero_pattern do
  tomtom = zipWithAdd 32, tomtom, (ring 0)
  rimrim = zipWithAdd 32, rimrim, (ring 0)
  hihatd = zipWithAdd 32, hihatd, (ring 0)
  hihatc = zipWithAdd 32, hihatc, (ring 0)
  clapss = zipWithAdd 32, clapss, (ring 0)
  snares = zipWithAdd 32, snares, (ring 0)
  kickss = zipWithAdd 32, kickss, (ring 0)
end
define :one_pattern do
  tomtom = zipWithAdd 32, tomtom, (ring 1)
  rimrim = zipWithAdd 32, rimrim, (ring 1)
  hihatd = zipWithAdd 32, hihatd, (ring 1)
  hihatc = zipWithAdd 32, hihatc, (ring 1)
  clapss = zipWithAdd 32, clapss, (ring 1)
  snares = zipWithAdd 32, snares, (ring 1)
  kickss = zipWithAdd 32, kickss, (ring 1)
end
define :e32th_pattern do
  tomtom = zipWithAdd 32, tomtom, (ring 1,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0)
  rimrim = zipWithAdd 32, rimrim, (ring 1,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0)
  hihatd = zipWithAdd 32, hihatd, (ring 1,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0)
  hihatc = zipWithAdd 32, hihatc, (ring 1,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0)
  clapss = zipWithAdd 32, clapss, (ring 1,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0)
  snares = zipWithAdd 32, snares, (ring 1,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0)
  kickss = zipWithAdd 32, kickss, (ring 1,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0)
end
define :e16th_pattern do
  e32th_pattern
  offset_pattern 16
  e32th_pattern
end
define :e8th_pattern do
  e16th_pattern
  offset_pattern 8
  e16th_pattern
end
define :e4th_pattern do
  e8th_pattern
  offset_pattern 4
  e8th_pattern
end
define :e2th_pattern do
  e4th_pattern
  offset_pattern 2
  e4th_pattern
end

define :ukgarage_pattern do
  tomtom = zipWithAdd 32, tomtom, (ring 0,0,0,0, 0,1,0,0, 0,0,0,1, 0,0,0,0)
  rimrim = zipWithAdd 32, rimrim, (ring 0,1,0,0, 0,0,0,1, 0,0,0,0, 0,1,0,0, 0,0,0,0, 0,0,0,1, 0,0,0,0, 0,1,0,0)
  hihatd = zipWithAdd 32, hihatd, (ring 0)
  hihatc = zipWithAdd 32, hihatc, (ring 0,0,1,1, 0,0,1,0, 0,0,1,0, 0,0,1,1, 0,0,1,0, 0,0,1,0, 0,0,1,0, 0,0,1,0)
  clapss = zipWithAdd 32, clapss, (ring 0,0,0,0, 1,0,0,0)
  snares = zipWithAdd 32, snares, (ring 0)
  kickss = zipWithAdd 32, kickss, (ring 1,0,0,0, 0,0,0,0, 0,0,1,0, 0,0,0,0)
end
define :dubstep_pattern do
  tomtom = zipWithAdd 32, tomtom, (ring 0)
  rimrim = zipWithAdd 32, rimrim, (ring 0)
  hihatd = zipWithAdd 32, hihatd, (ring 0,0,0,0, 1,0,0,0)
  hihatc = zipWithAdd 32, hihatc, (ring 0,1,1,0, 0,0,1,0, 0,0,0,1, 0,0,1,0)
  clapss = zipWithAdd 32, clapss, (ring 0)
  snares = zipWithAdd 32, snares, (ring 0,0,0,0, 0,0,0,0, 1,0,0,0, 0,0,0,0)
  kickss = zipWithAdd 32, kickss, (ring 1,0,0,0, 0,0,0,0, 0,0,1,0, 0,0,0,0, 1,0,0,1, 0,0,1,0, 0,0,1,0, 0,0,0,0)
end
define :footwork_pattern do
  tomtom = zipWithAdd 32, tomtom, (ring 0)
  rimrim = zipWithAdd 32, rimrim, (ring 1)
  hihatd = zipWithAdd 32, hihatd, (ring 0)
  hihatc = zipWithAdd 32, hihatc, (ring 0,0,1,0, 0,0,0,0, 0,0,1,0, 0,0,0,0, 0,0,1,0, 0,0,1,1, 0,0,1,0, 0,0,1,0)
  clapss = zipWithAdd 32, clapss, (ring 0,0,0,0, 0,0,0,0, 0,0,0,0, 1,0,0,0)
  snares = zipWithAdd 32, snares, (ring 0)
  kickss = zipWithAdd 32, kickss, (ring 1,0,0,1, 0,0,1,0)
end
define :trap_pattern do
  tomtom = zipWithAdd 32, tomtom, (ring 0)
  rimrim = zipWithAdd 32, rimrim, (ring 0)
  hihatd = zipWithAdd 32, hihatd, (ring 0)
  hihatc = zipWithAdd 32, hihatc, (ring 1,0,1,0)
  clapss = zipWithAdd 32, clapss, (ring 0)
  snares = zipWithAdd 32, snares, (ring 0,0,0,0, 0,0,0,0, 1,0,0,0, 0,0,0,0)
  kickss = zipWithAdd 32, kickss, (ring 1,0,0,0, 0,0,1,0, 0,0,0,0, 1,0,0,0, 0,0,1,0, 1,0,0,0, 0,0,0,0, 0,0,0,0)
end
define :drumNbass_pattern do
  tomtom = zipWithAdd 32, tomtom, (ring 0)
  rimrim = zipWithAdd 32, rimrim, (ring 0)
  hihatd = zipWithAdd 32, hihatd, (ring 1,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0)
  hihatc = zipWithAdd 32, hihatc, (ring 1,0)
  clapss = zipWithAdd 32, clapss, (ring 0)
  snares = zipWithAdd 32, snares, (ring 0,0,0,0, 1,0,0,0, 0,0,1,0, 1,0,0,0, 0,0,0,0, 1,0,0,0, 0,0,0,0, 1,0,0,0)
  kickss = zipWithAdd 32, kickss, (ring 1,0,0,0, 0,0,1,0, 0,0,0,0, 0,0,0,0)
end
define :jungle_pattern do
  tomtom = zipWithAdd 32, tomtom, (ring 0)
  rimrim = zipWithAdd 32, rimrim, (ring 0)
  hihatd = zipWithAdd 32, hihatd, (ring 1,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0)
  hihatc = zipWithAdd 32, hihatc, (ring 1,0)
  clapss = zipWithAdd 32, clapss, (ring 0)
  snares = zipWithAdd 32, snares, (ring 0,0,0,0, 1,0,0,1, 0,1,0,0, 0,0,1,0, 0,1,0,0, 1,0,0,1, 0,1,0,0, 0,0,1,0)
  kickss = zipWithAdd 32, kickss, (ring 1,0,1,0, 0,0,0,0, 0,0,1,0, 0,0,0,0, 0,1,1,0, 0,0,0,0, 0,0,1,0, 0,0,0,0)
end
define :deepHouse_pattern do
  tomtom = zipWithAdd 32, tomtom, (ring 0)
  rimrim = zipWithAdd 32, rimrim, (ring 0)
  hihatd = zipWithAdd 32, hihatd, (ring 0,0,1,0)
  hihatc = zipWithAdd 32, hihatc, (ring 0,1,0,0, 0,0,0,1, 0,1,0,0, 0,0,0,0)
  clapss = zipWithAdd 32, clapss, (ring 0,0,0,0, 1,0,0,0)
  snares = zipWithAdd 32, snares, (ring 0)
  kickss = zipWithAdd 32, kickss, (ring 1,0,0,0)
end
define :hiphop_pattern do
  tomtom = zipWithAdd 32, tomtom, (ring 0)
  rimrim = zipWithAdd 32, rimrim, (ring 0)
  hihatd = zipWithAdd 32, hihatd, (ring 0)
  hihatc = zipWithAdd 32, hihatc, (ring 1,0)
  clapss = zipWithAdd 32, clapss, (ring 0)
  snares = zipWithAdd 32, snares, (ring 0,0,0,0, 1,0,0,0)
  kickss = zipWithAdd 32, kickss, (ring 1,0,1,0, 0,0,0,0, 0,0,1,1, 0,0,0,1)
end
define :offset_pattern do |n|
  tomtom = offset 32, n, tomtom
  rimrim = offset 32, n, rimrim
  hihatd = offset 32, n, hihatd
  hihatc = offset 32, n, hihatc
  clapss = offset 32, n, clapss
  snares = offset 32, n, snares
  kickss = offset 32, n, kickss
end



define :add do |a, b|
  add = a + b
end
define :zipWithAdd do |length, a, b|
  zipWithAdd = zipWith length, :add, a, b
end




define :zipWith do |length, f, a, b|
  
  res = (ring)
  
  tick_reset :over
  length.times do
    tick(:over)
    res = res + (ring (method(f).call(a.look(:over), b.look(:over))))
  end
  
  #return the res list
  zipWith = res
end
define :fmap do |length, f, a|
  
  res = (ring)
  
  tick_reset :over
  length.times do
    tick(:over)
    res = res + (ring (method(f).call(a.look(:over))))
  end
  
  #return the res list
  fmap = res
end
define :offset do |length, n, a|
  
  res = (ring)
  
  tick_reset :over
  n.times do
    tick(:over)
  end
  
  length.times do
    tick(:over)
    res = res + (ring a.look(:over))
  end
  
  #return the res list
  offset = res
end
