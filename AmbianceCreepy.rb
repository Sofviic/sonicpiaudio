use_debug false
t = 0
live_loop :tick do
  sleep 0.1
  t=t+1
  puts "t = ", t
  puts "t%15 = ", t % 15
end
live_loop :ambiance do
  sync :tick
  sample :ambi_dark_woosh, amp: 0.02, rate: (rrand 0.125, 1.5)
end
live_loop :mainline do
  sync :tick
  the_lick if t % 60 == 0
end
live_loop :drums do
  sync :tick
  if t % 20 == 0
    sample :drum_bass_hard, amp: 0.05, rate: (rrand 0.0125, 0.15) if t % 7 == 0
    sample :drum_snare_hard, amp: 0.05, rate: (rrand 0.0125, 0.15)
  end
end
define :the_lick do
  if (rrand 0, 1) < 0.3
    mel_amp = 0.005
    offset = 12 + rrand_i(-12, 12)
    offset3 = choose [0,2,4,5,7]
    licknotes = [:e5,:fs5,:g5,:a5,:fs5,:d5,:e5]
    the_lick_chords = [(chord licknotes[0] + offset, :minor),
                       (chord licknotes[1] + offset, :major),
                       (chord licknotes[2] + offset + offset3, :minor),
                       (chord licknotes[3] + offset, :major),
                       (chord licknotes[4] + offset, :minor),
                       (chord licknotes[5] + offset + offset3, :major),
                       (chord licknotes[6] + offset, :minor)]
    puts the_lick_chords
    the_lick_dur = [0.25,0.25,0.25,0.25,0.5,0.25,0.75]
    puts the_lick_dur
    with_fx :reverb, room: 1, damp: 0.4 do
      with_fx :echo, phase: 0.3, decay: 4 do
        with_fx :echo, phase: 0.3, decay: 4 do
          play_pattern_timed the_lick_chords, the_lick_dur, amp: mel_amp, sustain: 0
        end
      end
    end
  end
end

